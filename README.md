# mean-app

MEAN application with registration and authorization based on JWT.

This is refactored version of mean-app. https://github.com/fermersky/mean-app <br>
Structure of application was organized using featrue-based approach. There are own module and routing module of every feature.
Also added core and shared modules. Core module has a guard which deny to use it more then one time.
